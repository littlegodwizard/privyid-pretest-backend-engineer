from fastapi import APIRouter

from api.routes import user, auth, balance


router = APIRouter()

router.include_router(user.router, tags=["user"], prefix="/user")
router.include_router(auth.router, tags=["auth"], prefix="/auth")
router.include_router(balance.router, tags=["balance"], prefix="/balance")
