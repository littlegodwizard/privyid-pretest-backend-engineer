from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from starlette.exceptions import HTTPException
from starlette.middleware.cors import CORSMiddleware
from api.errors.http_error import http_error_handler
from api.errors.validation_error import http422_error_handler
from api.errors.authentication_error import authentication_error_handler
from api.routes.api import router as api_router
from db.mongodb import connect_to_mongo
from fastapi_jwt_auth.exceptions import AuthJWTException


def get_application() -> FastAPI:
    application = FastAPI(title="privyid-pretest-backend enginner")

    application.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_origin_regex='https?://.*',
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    application.add_event_handler("startup", connect_to_mongo)
    # application.add_event_handler("shutdown", create_stop_app_handler(application))

    application.add_exception_handler(HTTPException, http_error_handler)
    application.add_exception_handler(RequestValidationError, http422_error_handler)
    application.add_exception_handler(AuthJWTException, authentication_error_handler)

    application.include_router(api_router)

    return application


app = get_application()