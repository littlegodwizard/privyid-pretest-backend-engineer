from enum import Enum
from typing import Optional

from fastapi import Depends
from pymongo.collection import Collection

from db.mongodb import get_collection
from models.helpers.mixin import IDModelMixin, DateTimeModelMixin, LocationModelMixin
from models.helpers.utils import RWModel, BSONObjectID


class TypeEnum(str, Enum):
    credit = 'CR'
    debit = 'DB'


class Info(RWModel ):
    activity: str
    # ip: Optional[str] = None
    # location: Optional[str] = None
    user_agent: Optional[str] = None
    type: TypeEnum
    author: str


class UserBalanceHistory(Info):
    balance_before: float
    balance_after: float


class UserBalanceHistoryInDB(IDModelMixin, DateTimeModelMixin, LocationModelMixin, UserBalanceHistory):
    user_balance_id: BSONObjectID


class UserBalanceHistoryDao:
    def __init__(self):
        self.dao: Collection = Depends(get_collection("user_balance_history")).dependency

    def add_user_balance_history(self, user_balance_history: UserBalanceHistoryInDB):
        insert = self.dao.insert_one(user_balance_history.dict())
        return insert.inserted_id
