from typing import Optional

from bson import ObjectId
from fastapi import Depends
from pydantic import Field, EmailStr
from pymongo.collection import Collection

from db.errors import EntityDoesNotExist
from db.mongodb import get_collection
from models.helpers.utils import RWModel, BSONObjectID
from modules.authentication import security
from models.user_balance import UserBalance
from models.helpers.mixin import IDModelMixin, DateTimeModelMixin


class User(RWModel):
    username: str
    email: str


class UserInDB(IDModelMixin, DateTimeModelMixin, User):
    salt: str = ""
    hashed_password: str = ""

    def check_password(self, password: str) -> bool:
        return security.verify_password(self.salt + password, self.hashed_password)

    def change_password(self, password: str) -> None:
        self.salt = security.generate_salt()
        self.hashed_password = security.get_password_hash(self.salt + password)


class UserInLogin(RWModel):
    email: EmailStr
    password: str


class UserInCreate(UserInLogin):
    username: str


class UserInUpdate(RWModel):
    id: Optional[BSONObjectID] = Field(None, alias='_id')
    username: Optional[str] = None
    email: Optional[EmailStr] = None
    password: Optional[str] = None


class UserWithToken(User):
    id: str
    token: str


class UserWithBalance(User):
    id: BSONObjectID = Field(None, alias='_id')
    balance: UserBalance


class UserInResponse(RWModel):
    user: UserWithToken


class UserDao:
    def __init__(self):
        self.dao: Collection = Depends(get_collection("users")).dependency

    def get_user_by_id(self, id: ObjectId):
        user = self.dao.aggregate([
            {"$match": {"_id": id}},
            {"$lookup": {
                "from": "user_balance",
                "let": {"id": "$_id"},
                "pipeline": [
                    {"$match": {"$expr": {"$eq": ["$user_id", "$$id"]}}},
                ],
                "as": "balance"
            }},
            {"$addFields": {
                "balance": {"$arrayElemAt": ["$balance", 0]}
            }}
        ])
        if user is not None:
            records = list(record for record in user)
            result = records[0]
            return UserWithBalance(**result)
        return None

    def create_new_user(self, user: UserInDB):
        insert = self.dao.insert_one(user.dict())
        return str(insert.inserted_id)

    async def get_user_by_username(self, username: str) -> UserInDB:
        user = self.dao.find_one({"username": username})
        if user is not None:
            return UserInDB(**user)

        raise EntityDoesNotExist(
            "user with username {0} does not exist".format(username),
        )

    async def get_user_by_email(self, email: str) -> UserInDB:
        user = self.dao.find_one({"email": email})
        if user is not None:
            return UserInDB(**user)

        raise EntityDoesNotExist(
            "user with email {0} does not exist".format(email),
        )
